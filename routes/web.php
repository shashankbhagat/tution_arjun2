<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function() { 
Route::get('/', function () {
    return view('home');
});
Route::get('/videoplayer', 'VideoController@index');
Route::get('/courses', 'CourseController@index');
Route::get('/course/{id}', 'CourseController@getSingleCourse');
Route::get('/takeTest/{cid}', 'CourseController@takeTest');
Route::post('/submitTest/submit', 'CourseController@submitTest');
Route::post('/submitTest/submit','CourseController@submitTest')->name('submitYourTest');
Route::get('/ratings/submit/{cid}/{rating}/{comments}', 'RatingController@store');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
