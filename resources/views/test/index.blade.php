@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
      <form method="POST" enctype="multipart/form-data" action="{{ route('submitYourTest') }}">
         @csrf
         <?php 
         $count = 0;
         ?>
         <input type="hidden" name="course_id" value="{{$course_id}}">
         <input type="hidden" name="0" value="0">
         @foreach($questions as $question)
            <?php
            $count = $count +1;
            ?>
         <input type="hidden" name="total_questions" value="<?php echo $count; ?>">
            <div class="card">
               <div class="card-header">
                  {{$question->slno}} . {{$question->question}}
               </div>
               <div class="card-body">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="">
                              <input type="radio" name="{{$question->id}}" value="{{$question->option1}}" required> {{$question->option1}}
                           </div>
                           <div class="">
                              <input type="radio" name="{{$question->id}}" value="{{$question->option2}}" required> {{$question->option2}}
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="">
                              <input type="radio" name="{{$question->id}}" value="{{$question->option3}}" required> {{$question->option3}}
                           </div>
                           <div class="">
                              <input type="radio" name="{{$question->id}}" value="{{$question->option4}}" required> {{$question->option4}}
                           </div>
                        </div>
                     </div>
               </div>
            </div>
         <br><br>
         @endforeach
         <button type="submit" class="btn btn-success">Submit</button>
      </form>
      </div>
   </div>
</div>
@endsection