<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Rating;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $cid, $star, $comments)
    {
        //
        $rating = new Rating;
        $rating->created_by = Auth::user()->id;
        $rating->course_id = $cid;
        $rating->rating = $star;
        $rating->comments = $comments;
        $rating->save();

        $data['rating'] = Rating::where('created_by', Auth::user()->id)->orderBy('id', 'DESC')->first()->toArray();

        $data['myRating'] = Rating::where('course_id', $cid)->where('created_by', Auth::user()->id)
                            ->orderBy('id', 'DESC')->first();
                            // dd($myRating);
        $avg = Rating::where('course_id', $cid)->avg('rating');
        $data['average'] = (int)$avg;

        return response()->json([
            'statusCode' => 200,
            'message' => 'Ratings submitted',
            'success' => $data,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
