@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12" style="text-align: center;">
        
<iframe width="95%" height="356" src="{{$url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
@endsection
