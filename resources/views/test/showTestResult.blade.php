@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <?php 
         $count = 0;
         ?>
         @foreach($questions as $key => $question)
            <?php
            $count = $count +1;
            ?>
         <input type="hidden" name="total_question" value="<?php echo $count; ?>">
            <div class="card">
               <div class="card-header">
                  {{$question['slno']}} . {{$question['question']}}
                  <div class="float-right">
                     @if($question['correct'] == $answers[$question['id']])
                        <i class="fas fa-check-circle "  style="color:green"></i>
                     @else
                        <i class="fas fa-times " style="color:red"></i>
                     @endif
                  </div>
               </div>
               <div class="card-body">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="">
                              Correct Answer : {{$question['correct']}}
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="">
                              Your Answer : {{$answers[$question['id']]}}
                           </div>
                        </div>
                     </div>
               </div>
               
            </div>
         <br><br>
         @endforeach
      </div>
   </div>
</div>
@endsection