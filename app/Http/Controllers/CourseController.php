<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\Video;
use App\Rating;
use Auth;
use App\Question;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $courses = Course::get();
        return view('course.index')->with([
            'courses'=>$courses,
        ]);
    }

    public function getSingleCourse($cid)
    {
        $course = Course::where('id', $cid)->first();
        $videos = Video::where('course_id', $cid)->orderBy('id', 'ASC')->paginate(8);
        $myRating = Rating::where('course_id', $cid)->where('created_by', Auth::user()->id)
                            ->orderBy('id', 'DESC')->first();
                            // dd($myRating);
        $avg = Rating::where('course_id', $cid)->avg('rating');
        $average = (int)$avg;

        return view('course.getSingleCourse')->with([
            'course'=>$course,
            'videos'=>$videos,
            // 'videos'=>$course->videos,
            'myRating'=>$myRating,
            'average'=>$average,
        ]);
    }

    public function takeTest($cid)
    {
        $questions = Question::where('course_id', $cid)->orderBy('slno', 'ASC')->get();

        return view('test.index')->with([
            'questions' => $questions,
            'course_id' => $cid,
        ]);
    }

    public function submitTest(Request $request)
    {   
        // dd('here');
        // dd($request->all());
        $data = [];

        $questions = Question::where('course_id', $request['course_id'])->get()->toArray();

        foreach ($questions as $question) {
            $data[$question['id']] = $request[$question['id']];
        }

        // dd($data);
        return view('test.showTestResult')->with([
            'questions' => $questions,
            'answers' => $data,
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
