-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2019 at 05:18 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arjun`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `duration` int(11) DEFAULT NULL,
  `fee` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `created_at`, `updated_at`, `deleted_at`, `status`, `created_by`, `name`, `duration`, `fee`, `certification`, `description`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 'Java ', 4, '5000', 'Yes', 'Learn java in 3 hrours'),
(2, NULL, NULL, NULL, NULL, NULL, 'Learn C', 3, '5000', 'Yes', 'Learn Cin 3 hrours'),
(3, NULL, NULL, NULL, NULL, NULL, 'Digital Marketing', 4, '5000', 'Yes, Google Certification', 'Learn DMin 3 hrours'),
(4, NULL, NULL, NULL, NULL, NULL, 'Hadoop', 4, '10000', 'Yes, ISO', 'Learn hadoop in 3 hrours');

-- --------------------------------------------------------

--
-- Table structure for table `format`
--

CREATE TABLE `format` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `slno` tinyint(4) DEFAULT NULL,
  `question` text COLLATE utf8mb4_unicode_ci,
  `option1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `created_at`, `updated_at`, `deleted_at`, `status`, `created_by`, `course_id`, `slno`, `question`, `option1`, `option2`, `option3`, `option4`, `correct`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Who invented java ?', 'Mark twain', 'Jame gosling', 'Robert Hawkins', 'Fedro', 'Jame gosling'),
(2, NULL, NULL, NULL, NULL, NULL, 1, 2, 'how many pewitive data types are in java ?', '6', '7', '8', '9', '8'),
(3, NULL, NULL, NULL, NULL, NULL, 1, 3, 'size of int in java is ?', '16 Bit', '32 Bit', '64 Bit', 'depends on exicution environment', '32 Bit'),
(4, NULL, NULL, NULL, NULL, NULL, 1, 4, 'size of float and double in java is ?', '32 and 64', '64 and 64', '32 and 32', '64 and 32', '32 and 64'),
(5, NULL, NULL, NULL, NULL, NULL, 1, 5, 'which of the following automatic type convertion will be possible ?', 'short to int', 'byte to int', 'int to long', 'long to int', 'int to long');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `created_at`, `updated_at`, `deleted_at`, `status`, `created_by`, `course_id`, `rating`, `comments`) VALUES
(1, '2019-05-14 05:24:21', '2019-05-14 05:24:21', NULL, NULL, 2, 1, 4, 'very good course very good course very good course very good course'),
(2, '2019-05-14 05:24:49', '2019-05-14 05:24:49', NULL, NULL, 2, 1, 1, 'very good course very good course very good course very good course'),
(3, '2019-05-14 05:57:19', '2019-05-14 05:57:19', NULL, NULL, 1, 1, 3, 'wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd wcd'),
(4, '2019-05-14 06:02:26', '2019-05-14 06:02:26', NULL, NULL, 1, 1, 4, 'nj'),
(5, '2019-05-18 00:03:48', '2019-05-18 00:03:48', NULL, NULL, 1, 2, 3, 'good ony what to tell'),
(6, '2019-05-18 00:03:58', '2019-05-18 00:03:58', NULL, NULL, 1, 2, 1, 'yes nice only'),
(7, '2019-05-18 00:07:47', '2019-05-18 00:07:47', NULL, NULL, 1, 2, 2, 'shhjsd'),
(8, '2019-05-18 00:54:06', '2019-05-18 00:54:06', NULL, NULL, 1, 3, 3, 'dsgvshvs sdvgsh'),
(9, '2019-05-18 00:54:57', '2019-05-18 00:54:57', NULL, NULL, 1, 3, 1, 'dhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssddhsbchjsc ssd'),
(10, '2019-05-23 21:08:22', '2019-05-23 21:08:22', NULL, NULL, 3, 2, 4, 'Very good course'),
(11, '2019-05-23 21:08:36', '2019-05-23 21:08:36', NULL, NULL, 3, 2, 2, 'ds'),
(12, '2019-05-23 21:08:42', '2019-05-23 21:08:42', NULL, NULL, 3, 2, 5, 'dsc');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'student', 'Arjun', 'arjun@shashankbhagat.com', NULL, '$2y$10$ARedukxAKBp/lhYoJQ5pTuKLFEJ05hKsZxolConjzk4yJSxKTvfKW', 'C84GuJ4RLELDOONXW3JWJuoQXh7O7Kg3gkVmcHUK64JmcLU4T5YWmH4qKxFC', '2019-05-13 06:12:39', '2019-05-13 06:12:39'),
(2, 'student', 'Shyam', 'shyam@shashankbhagat.com', NULL, '$2y$10$4ZyqK67Ve7sYexrEiRomzuB5ukT0Kv/EuYT9C7HLyOTALDa6X/m7i', NULL, '2019-05-14 05:18:57', '2019-05-14 05:18:57'),
(3, 'student', 'Mallikarjuna', 'pobbathimalli202@gmail.com', NULL, '$2y$10$9HZUGX3hEwSQtQwLtrb0xO5LhsyYPSYBMwdUttMtx.ebVWTVn/Pcm', NULL, '2019-05-23 21:04:32', '2019-05-23 21:04:32');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `slno` tinyint(4) DEFAULT NULL,
  `topic_name` text COLLATE utf8mb4_unicode_ci,
  `topic_description` text COLLATE utf8mb4_unicode_ci,
  `video_link` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `created_at`, `updated_at`, `deleted_at`, `status`, `created_by`, `course_id`, `slno`, `topic_name`, `topic_description`, `video_link`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 1, 1, 'JAVA BEGINNERS', '', 'https://www.youtube.com/embed/5W568dNIVPM'),
(2, NULL, NULL, NULL, NULL, NULL, 1, 2, 'HOW TO BE A JAVA PROGRAMMER', '', 'https://www.youtube.com/embed/WOUpjal8ee4'),
(3, NULL, NULL, NULL, NULL, NULL, 1, 3, 'PREREQUISITES TO LEARN JAVA', '', 'https://www.youtube.com/embed/9_CJYK47d6M'),
(4, NULL, NULL, NULL, NULL, NULL, 1, 4, 'SOFTWARE REQUIREMENTS FOR JAVA', '', 'https://www.youtube.com/embed/9Kk1dGkpGPI'),
(5, NULL, NULL, NULL, NULL, NULL, 1, 5, 'SIMPLE EXAMPLE CODE IN JAVA', '', 'https://www.youtube.com/embed/E-LMhKEwL_w'),
(6, NULL, NULL, NULL, NULL, NULL, 2, 1, 'INTRUDUCTION OF C LANGUAGE', '', 'https://www.youtube.com/embed/si-KFFOW2gw'),
(7, NULL, NULL, NULL, NULL, NULL, 2, 2, 'PLATFORM DEPENDANCY IN C LANGUAGE', '', 'https://www.youtube.com/embed/nZnMH5x6PfQ'),
(8, NULL, NULL, NULL, NULL, NULL, 2, 3, 'VARIABLES IN C', '', 'https://www.youtube.com/embed/rFnWoTnoC50'),
(9, NULL, NULL, NULL, NULL, NULL, 2, 4, 'FUNCTIONS IN C', '', 'https://www.youtube.com/embed/TxWKGhF9KdM'),
(10, NULL, NULL, NULL, NULL, NULL, 2, 5, 'DATATYPES IN C', '', 'https://www.youtube.com/embed/j1u3V6pzwEI'),
(11, NULL, NULL, NULL, NULL, NULL, 3, 1, 'DIGITAL MARKETING FOR BEGINERS', '', 'https://www.youtube.com/embed/xA_yMYN19ug'),
(12, NULL, NULL, NULL, NULL, NULL, 3, 2, 'EMAIL MARKETING', '', 'https://www.youtube.com/embed/RkiX6zcjJBk'),
(13, NULL, NULL, NULL, NULL, NULL, 3, 3, 'MOBILE MARKETING', '', 'https://www.youtube.com/embed/FX9GsblIn9k'),
(14, NULL, NULL, NULL, NULL, NULL, 3, 4, 'DIGITAL MARKETING ANALYTICS', '', 'https://www.youtube.com/embed/WaDjcajOmUo'),
(15, NULL, NULL, NULL, NULL, NULL, 3, 5, 'DIGITAL MARKETING STATEGIES', '', 'https://www.youtube.com/embed/hZLMv5aexto'),
(16, NULL, NULL, NULL, NULL, NULL, 4, 1, 'BIGDATA TUTORIALS FOR BEGINERS', '', 'https://www.youtube.com/embed/zez2Tv-bcXY'),
(17, NULL, NULL, NULL, NULL, NULL, 4, 2, 'HOW TO BECOME A BIGDATA ENGINEER', '', 'https://www.youtube.com/embed/GRFQxd_0k3M'),
(18, NULL, NULL, NULL, NULL, NULL, 4, 3, 'INTRUDUCTION TO HADOOP', '', 'https://www.youtube.com/embed/rqVuj4nft-E'),
(19, NULL, NULL, NULL, NULL, NULL, 4, 4, 'HADOOP ECOSYSTEM', '', 'https://www.youtube.com/embed/-XkEX1onpEI'),
(20, NULL, NULL, NULL, NULL, NULL, 4, 5, 'HADOOP ARCHITECTURE', '', 'https://www.youtube.com/embed/-XkEX1onpEI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `format`
--
ALTER TABLE `format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
