@extends('layouts.app')
@section('beforeStyles')
<style>
   .checked {
   color: orange;
   }
   
   textarea {
   border-left: none;
   border-right: none;
   border-top: none;
   border-bottom: 1px solid black;
   margin-top: 0px;
   margin-bottom: 0px;
   /*height: 79px;*/
   resize: none;
   width: 100%;
   }

   textarea:focus {
      outline: none ;
   }
   .scroll-info {
         text-align: center;
         margin: 0 auto;
         position: absolute;
         bottom: 25px;
         width: 100%;
      }
      .modal-scroll-info {
         /* width: 50%; */
         width: 280px;
         display: block;
         padding: 5px 5px;
         background: #c7ecee;
         text-align: center;
         border-radius: 3px;
         margin: 0 auto;
         position: absolute;
         /* bottom: 9px; */
         top: 50%;
         left: 50%;
         transform: translate(-50%,-50%);
         height: 29px;
         color: #111;
         font-size: 13px;
         transition: .35s;
      }
      .modal-scroll-info:hover, .modal-scroll-info:active {
         text-decoration: none;
      }
</style>
@endsection
@section('content')
<div class="container">
   <div class="row ">
      <div class="col-md-8">
         <div class="card card-header">{{$course->name}}</div>
         <br>
      <div class="activity_wrap" id="flux" style="overflow-y: scroll; max-height: 52vh">
         @foreach($videos as $key => $video)
         <div class="card">
            <div class="card-header">
               {{$video->slno}} . {{$video->topic_name}}
               <div class="float-right">
                  <a href="/videoplayer?url={{$video->video_link}}"><span class="badge badge-secondary">Watch Video</span></a>
               </div>
            </div>
            <div class="card-body">
               <div class="row">
                  <div class="col-md-8">
                     {{$video->topic_description}}
                  </div>
                  <div class="col-md-12">
                     <!-- <video height="150px" src="{{$video->video_link}}" type='video/x-matroska; codecs="theora, vorbis"' controls onerror="failed(event)" ></video> -->
                     <!-- <iframe width="420" height="315" src="{{$video->video_link}}">
                     </iframe> -->
                  </div>
               </div>
            </div>
         </div>
         <br>
         @endforeach
         <div class="alert alert-success">
            <a href="/takeTest/{{$course->id}}"> Take test </a>
         </div>


      </div>
      <br>
         {{$videos->links()}}
         <div class="scroll-info">
                     <a href="#end-of-div" class="modal-scroll-info">
                        <i class="fas fa-arrow-down"></i> Please scroll-down to view more.
                     </a>
                  </div>
      </div>
      <div class="col-md-4">
         <div class="row">
            <div class="col">
               <div class="card">
                  <div class="card-header avg-rating">
                     Average Ratings 
                     @for($i=1; $i<=5; $i++)
                     @if($i <= $average)
                     <span class="fa fa-star checked"></span> 
                     @else
                     <span class="fa fa-star"></span> 
                     @endif
                     @endfor
                  </div>
               </div>
            </div>
         </div>
         <br>
         <div class="row prev-row" style="display: {{$myRating ? 'block' : 'none' }};">
            <div class="col">
               <div class="card">
                  <div class="card-header prev-rating">
                     @if($myRating)
                     Your previous rating
                     @for($i=1; $i<=5; $i++)
                     @if($i <= $myRating->rating)
                     <span class="fa fa-star checked"></span> 
                     @else
                     <span class="fa fa-star"></span> 
                     @endif
                     @endfor
                     @endif
                  </div>
                  <div class="card-body prev-comments">
                     @if($myRating)
                     {{$myRating->comments}}
                     @endif
                  </div>
               </div>
            </div>
         </div>
         <br>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     Rate now : 
                     <span class="fa fa-star astar cstar1" data-id="1"></span>
                              <span class="fa fa-star astar cstar2" data-id="2"></span>
                              <span class="fa fa-star astar cstar3" data-id="3"></span>
                              <span class="fa fa-star astar cstar4" data-id="4"></span>
                              <span class="fa fa-star astar cstar5" data-id="5"></span>

                              <button type="button" class="btn btn-sm btn-primary float-right rating-submit"> Submit</button>

                  </div>
                  <div class="card-body">
                     <form>
                        @csrf
                        <input type="hidden" class="rating" name="rating" value=""/>
                        <input type="hidden" class="course_id" name="course_id" value="{{$course->id}}"/>
                        <div class="row">
                           <div class="col-md-12">
                              <textarea class="comments" name="comments" rows="3" width="100%"></textarea><br>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col">
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('afterScripts')
<script>
   $( document ).ready(function() {
    console.log( "ready!" );
   });
   
   $('.astar').click(function() {
      var value = $(this).data('id');
      $('.rating').val(value);
      $('.astar').removeClass('checked');
   
      for (var i = 1; i <= value; i++) {
         $('.cstar'+i).addClass('checked');
      }



   });


   
   $('.rating-submit').click(function(){
      var rating = $('.rating').val();
      var course_id = $('.course_id').val();
      var comments = $('.comments').val();
      if (rating) {
         if (comments) {
            $.ajax({
         type: 'get',
         url : '/ratings/submit/'+course_id+'/'+rating+'/'+comments,
         success : function(data) {
            if(data.statusCode == 200) {
               $('.comments').val('');
               $('.astar').removeClass('checked');
               $('.rating').val('');
               var avgRating='Average Ratings ';
               for (var i = 1; i <= 5; i++) {
                  if (i<=data.success.average) {
                     avgRating+='<span class="fa fa-star checked"></span>';
                  } else {
                     avgRating+='<span class="fa fa-star"></span>';
                  }
               }
               var prevRating='Your previous rating ';
               for (var i = 1; i <= 5; i++) {
                  if (i<=data.success.rating.rating) {
                     prevRating+='<span class="fa fa-star checked"></span>';
                  } else {
                     prevRating+='<span class="fa fa-star"></span>';
                  }
               }
               $('.prev-comments').html(data.success.rating.comments);
               $('.avg-rating').html(avgRating);
               $('.prev-rating').html(prevRating);
               $('.prev-row').show();
               $.simplyToast('Rating submitted', 'success');
            }
         }
      });
         } else {
         $.simplyToast('Please fill the comments', 'danger');
         }
      } else {
         $.simplyToast('Please fill the ratings', 'danger');
      }
   });
   
   //Finding the end of Activity Wrap Area div
               var displayed = 0;
         $(function($) {
          $('.activity_wrap').on('scroll', function() {
              if(Math.round($(this).scrollTop() + $(this).innerHeight(), 10) >= Math.round($(this)[0].scrollHeight, 10)) {
                  // $('.activity_wrap .activity_wrap_loader').show();
                     if (displayed == 0) {
                     displayed = 1;

                     $.simplyToast('End reached', 'success');
                     }
                  // alert('End of Activity Wrap Area');
              }
          })
      });
</script>
@endsection