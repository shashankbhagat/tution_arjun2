@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         @foreach($courses as $course)
         <a href="/course/{{$course->id}}" style="text-decoration: none;">
            <div class="card">
               <div class="card-header">
                  {{$course->name}}
                  <!-- <div class="float-right"><span class="badge badge-primary">View</span></div> -->
                  <div class="float-right"><i class="fas fa-clock"></i> {{$course->duration}} hours</div>
               </div>
               <div class="card-body">
                  <div class="">
                     <span class="">Fee : </span>
                     <span class="">{{$course->fee}}</span>
                  </div>
                  <div class="">
                     <span class="">Certification : </span>
                     <span class="">{{$course->certification}}</span>
                  </div>
               </div>
            </div>
         </a>
         <br><br>
         @endforeach
      </div>
   </div>
</div>
@endsection